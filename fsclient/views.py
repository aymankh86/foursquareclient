from datetime import datetime
import json

from django.shortcuts import render
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import SearchHistory

import requests


def home(request):
    query = request.GET.get('query', None)
    near = request.GET.get('near', None)
    error = ""

    latest_searches = SearchHistory.objects.exclude(
        query=query, near=near).order_by('-created_at')[:20]

    context = {'latest_searches': latest_searches}

    if query and near:
        obj, created = SearchHistory.objects.get_or_create(
            query=query, near=near)
        if True:
            params = {
                'query': query,
                'near': near,
                'client_id': settings.FS_CLIENT_KEY,
                'client_secret': settings.FS_CLIENT_SECRET,
                'v': datetime.today().strftime('%Y%M%d')
            }
            r = requests.get(settings.FS_API_URL, params=params)
            if r.status_code == 200:
                data = r.json()
                obj.data = data['response']['venues']
                obj.save()
            else:
                obj.delete()
                error = "Something went wrong"

        if error:
            context.update({'query': query, 'near': near,
                            'error': error, 'venues': []})
        else:
            paginator = Paginator(obj.data, 10)
            page = request.GET.get('page', 1)
            try:
                venues = paginator.page(page)
            except PageNotAnInteger:
                venues = paginator.page(1)
            except EmptyPage:
                venues = paginator.page(paginator.num_pages)

            context.update({'query': query, 'near': near, 'venues': venues})

    return render(request, 'fsclient/home.html', context)
