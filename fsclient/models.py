from django.db import models
from jsonfield import JSONField


class SearchHistory(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    query = models.CharField(max_length=255)
    near = models.CharField(max_length=255)
    data = JSONField()

    def __str__(self):
        return self.query
