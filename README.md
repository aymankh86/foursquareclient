# README #


### What is this repository for? ###

* In this micro website, users can search for the places that they want to visit in a specific place using Foursquare api. For example someone interested in eating pizza in istanbul, He will type pizza and istanbul into two fields.

### How do I get set up? ###

* `virtualenv venv`
* `source venv/bin/activate`
* `git clone git@bitbucket.org:aymankh86/foursquareclient.git && cd foursquareclient`
* `pip install -r requirements.txt`
* `python manage.py runserver`


**Test online on:** [https://volt-ex.herokuapp.com/](https://volt-ex.herokuapp.com/)


**Admin login** (admin/admin123)